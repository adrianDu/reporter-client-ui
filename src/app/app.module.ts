import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule }    from '@angular/forms';


import { AppComponent } from './app.component';
import { TableComponent } from './components/table-component/table-component';
import { TreeTableComponent } from './components/table-component/treetable-component';
import { HNcontComponent } from './components/table-component/h-ncont-component';

import { TableComponentService } from './components/table-component/table-component-service';
import { NContShareDataService } from './components/shared/n-cont.sharedata.service';
import { SharedService } from './components/shared/shared-service';
import { SessionService } from './components/shared/session-service';
import { AppRoutingModule } from './app.routing';


import { HttpClientModule } from '@angular/common/http';
// import { HttpModule } from '@angular/http';
import { RestService } from './rest-service/rest-service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import {TreeTableModule} from 'primeng/treetable';

import { TreeModule  } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/primeng'; // Here
import { PaginatorModule } from 'primeng/primeng'; // Here
import { SharedModule } from 'primeng/primeng';
import {ButtonModule} from 'primeng/button';
@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    TreeTableComponent,
    HNcontComponent
  ],
   exports: [TreeTableComponent],
  imports: [
    AppRoutingModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    // HttpModule,
    TableModule,
    DropdownModule,
    BrowserAnimationsModule,
    TreeTableModule,
    TreeModule ,
    DataTableModule,
    PaginatorModule,
    SharedModule,
    ButtonModule
  ],
  bootstrap: [AppComponent],
  providers: [SessionService, TableComponentService, RestService, NContShareDataService, SharedService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }