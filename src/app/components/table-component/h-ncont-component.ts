import {Component, OnInit} from '@angular/core';
import { TableComponentService } from './table-component-service';
import { NCont } from '../../model/n-cont';
import { Page } from '../../model/page';
import { ContDto } from '../../model/cont-dto';
 
import { HttpClient } from '@angular/common/http';

import {LazyLoadEvent} from 'primeng/primeng';
import {SelectItem} from 'primeng/api';
import { Organisation } from '../../model/organisation';
import { RestService } from '../../rest-service/rest-service';
import {TreeNode} from 'primeng/api';
import { NContShareDataService } from '../shared/n-cont.sharedata.service';



@Component({
  selector: 'h-ncont-component',
  templateUrl: './h-ncont-component.html',
  //styleUrls: ['./table-component.css']
})
export class HNcontComponent  implements OnInit {
	ncontList: NCont[];
	ncontPage: Page<NCont>;
	jusList: string[] = ['1','2'];
    loading: boolean;

    // selectedOrganisationId: string = 'b596ac15-dfa5-441a-b764-96d9484a73f5';

    lunaAnFrom: SelectItem[];
    selectedLunaAnFrom: SelectItem;

    lunaAnTo: SelectItem[];
    selectedLunaAnTo: SelectItem;

    syncDateListFrom: SelectItem[];
    selectedSyncDateFrom: SelectItem;

    syncDateListTo: SelectItem[];
    selectedSyncDateTo: SelectItem;

    organisationList: SelectItem[];
    selectedOrganisation: SelectItem;

    conturiList: SelectItem[];
    selectedCont: SelectItem;

    selectedNcont: NCont;
	tableCols: any[];

	constructor(private tableComponentService: TableComponentService, private http: HttpClient,
				private restService: RestService,
				private shareDataService: NContShareDataService) { }
	
	ngOnInit() {
		console.log("ngOnInit:");
		this.tableCols = [
			{ field: 'denumire', header: 'Tip balanta' },
			{ field: 'clasa', header: 'Clasa' },
			{ field: 'dateCreate', header: 'Data' },
			{ field: '', header: 'Luna' },
			{ field: 'soldic', header: 'SoldID' },
			{ field: 'soldic', header: 'SoldIC' },
			{ field: 'curentd', header: 'RulajD' },
			{ field: 'curentc', header: 'RulajC' },
			{ field: 'precedentd', header: 'PrecedentD' },
			{ field: 'precedentc', header: 'PrecedentC' },
			{ field: 'nsoldid', header: 'SoldFD' },
			{ field: 'nsoldic', header: 'SoldFC' }
		];


		this.initData();

		//TODO delete
		this.ngOnInit2();
	}

//TODO delete
	// getNContList2(): void {
	// 	this.tableComponentService.getNContList3()
 //     	.subscribe((ncontListVar: any) => this.ncontList = ncontListVar);
	// }

	getNrOfRows(): number{
		if(this.ncontPage) {
			return (this.ncontPage.pageSize);
		}else{
			return 10;
		}
	}
	getTotalRecords(): number{
		if(this.ncontPage) {
			return (this.ncontPage.total);
		}else{
			return 0;
		}
	}

	loadLazy(event: LazyLoadEvent) {
        this.loading = true;
        // setTimeout(() => {
            if (this.tableComponentService) { 
            	console.log("loadLazy loadData");
            	console.log(this.selectedOrganisation.value, this.selectedLunaAnFrom.value, this.selectedLunaAnTo.value, 
					this.selectedSyncDateFrom.value, this.selectedSyncDateTo.value, this.selectedCont.value, (event.first/event.rows), this.getNrOfRows());           	
				this.loadData(this.selectedOrganisation.value, this.selectedLunaAnFrom.value, this.selectedLunaAnTo.value, 
					this.selectedSyncDateFrom.value, this.selectedSyncDateTo.value, this.selectedCont.value, (event.first/event.rows), this.getNrOfRows());
                this.loading = false;
            }
        // }, 1000);
    }

	isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}

    loadData(organisationId:string, selectedLunaAnFrom:string, selectedLunaAnTo:string, selectedSyncDateFrom:string, selectedSyncDateTo:string, 
    	selectedCont:string, page:number, pageSize:number) {
    	if(this.isEmpty(page)){
    		console.log("page ok");
    	}else{
    		console.log("page null");
    	}
		console.log("loadData:", organisationId, selectedLunaAnFrom, selectedLunaAnTo, selectedSyncDateFrom, selectedSyncDateTo, 
    	selectedCont, page, pageSize);
		if (organisationId && selectedLunaAnFrom && selectedLunaAnTo && selectedSyncDateFrom && selectedSyncDateTo && selectedCont && this.isEmpty(page) && pageSize) {

			console.log("loadData: NO null values");
	    	this.tableComponentService.getHNContList(organisationId, selectedLunaAnFrom, selectedLunaAnTo, selectedSyncDateFrom, selectedSyncDateTo, selectedCont, page, pageSize)
			     	.subscribe((ncontPage: Page<NCont>) => {this.ncontPage = ncontPage;
			     											console.log("ncontPage:" + JSON.stringify(ncontPage));
			     										   this.ncontList = ncontPage.items;
			     										   });
		}else {
			this.ncontList = [];
		}
    }

    initData() {
        console.log("initData................." );
        this.restService.getOrganisationList('1')//this.user.userId)
             .subscribe((organisationList: Organisation[]) => {
                           this.organisationList = [];
                           if(organisationList){
                             for(var i=0; i<organisationList.length; i++){
                               this.organisationList.push({label:organisationList[i].alias, value:organisationList[i].id});
                             }
                             console.log("organisationList:" + organisationList);  
                          }  
                        if(this.organisationList){
						    this.changeOrganisation(this.organisationList[0]);
						}else {
							this.changeOrganisation(null);
						}
             }); 
    	 	
    }

	changeOrganisation(itemChanged){
		if(itemChanged){
			console.log("changeOrganisation", itemChanged.label, itemChanged.value);
		} else {
			console.log("changeOrganisation null values");
		}
		this.selectedOrganisation = itemChanged;
		this.reloadConturi();
	}

	reloadConturi(){
		//TODO
        console.log("reloadConturi................." );
	  	if(this.selectedOrganisation && this.selectedOrganisation.value) {
	        this.tableComponentService.getConturiByOrganisationList(this.selectedOrganisation.value)
	             .subscribe((conturiList: ContDto[]) => {
	                           this.conturiList = [];
	                           if(conturiList){
	                             for(var i=0; i<conturiList.length; i++){
	                             	const labelCont = (conturiList[i].denumire + " " + conturiList[i].simbol);
	                               this.conturiList.push({label: labelCont, value:conturiList[i].simbol});
	                             }
	                             console.log("conturiList:" + conturiList);  
	                          }  
	                        if(this.conturiList){
							    this.changeCont(this.conturiList[0]);
							}else {
								this.changeCont(null);
							}
	             }); 
	    }else {
		  	this.conturiList = [];
		  	this.selectedCont = null;
		  	this.changeCont(null);
	    }
		
	}
	
	changeCont(itemChanged){
		if(itemChanged){
			console.log("changeCont", itemChanged.label, itemChanged.value);
		} else {
			console.log("changeCont null values");
		}
		this.selectedCont = itemChanged;
		this.reloadLunaAnData();
	}

	reloadLunaAnData (){  	
	  if(this.selectedOrganisation && this.selectedOrganisation.value) {
	  	 this.tableComponentService.getLunaAnByOrganisationList(this.selectedOrganisation.value)
	     	.subscribe((lunaAnList: string[]) => {
	     								this.lunaAnFrom = [];
	     								this.lunaAnTo = [];
					     				if(lunaAnList){
							     			for(var i=0; i<lunaAnList.length; i++){
							     				this.lunaAnFrom.push({label:lunaAnList[i], value:lunaAnList[i]});
							     				this.lunaAnTo.push({label:lunaAnList[i], value:lunaAnList[i]});
							     			}											
										}	
										console.log("lunaAnList:" + lunaAnList);
										if(lunaAnList){
											this.selectedLunaAnFrom = this.lunaAnFrom[0];
											this.selectedLunaAnTo = this.lunaAnTo[0];
										}else {
											this.selectedLunaAnFrom = null;
											this.selectedLunaAnTo = null;
										}
	  									this.reloadSyncDate();
	     });   
	  } else {
	  	this.lunaAnFrom = [];
	  	this.selectedLunaAnFrom = null;
	  	this.loadLunaAnFrom(null);
	  	this.lunaAnTo = [];
	  	this.selectedLunaAnTo = null;
	  	this.loadLunaAnTo(null);
	  }
	}

    loadLunaAnFrom(itemChanged) {
    	if(itemChanged){
    		console.log("loadLunaAn", itemChanged.label, itemChanged.value);
    	} else {
    		console.log("loadLunaAn null values");
    	}
		this.selectedLunaAnFrom = itemChanged;
      	this.reloadSyncDate();
    }

    loadLunaAnTo(itemChanged) {
    	if(itemChanged){
    		console.log("loadLunaAn", itemChanged.label, itemChanged.value);
    	} else {
    		console.log("loadLunaAn null values");
    	}
		this.selectedLunaAnTo = itemChanged;
      	this.reloadSyncDate();
    }

    reloadSyncDate(){
		if(this.selectedOrganisation && this.selectedOrganisation.value && this.selectedLunaAnFrom && this.selectedLunaAnFrom.value 
			&& this.selectedLunaAnTo && this.selectedLunaAnTo.value) {
			this.tableComponentService.getSyncDateListFromTo(this.selectedOrganisation.value, this.selectedLunaAnFrom.value, this.selectedLunaAnTo.value)
		     	.subscribe((syncDateListR: string[]) => {
		     								this.syncDateListFrom = [];
		     								this.syncDateListTo = [];
						     				if(syncDateListR){
								     			for(var i=0; i<syncDateListR.length; i++){
								     				this.syncDateListFrom.push({label:syncDateListR[i], value:syncDateListR[i]});
								     				this.syncDateListTo.push({label:syncDateListR[i], value:syncDateListR[i]});
	 											console.log("syncDateList: value"+ i + " =" + syncDateListR[i]);
								     			}
		     											// this.syncDateList = syncDateList;
	 											
											}
											console.log("syncDateList:" + syncDateListR);
 											if(syncDateListR){
										 		this.selectedSyncDateFrom = this.syncDateListFrom[0];
										 		this.selectedSyncDateTo = this.syncDateListTo[0];
 											}else {
										 		this.selectedSyncDateFrom = null;
										 		this.selectedSyncDateTo = null;
 											}
 											this.reloadData();
											
		     	});   
		 } else {
		    this.syncDateListFrom = [];
	 		this.selectedSyncDateFrom = null;
		    this.syncDateListTo = [];
	 		this.selectedSyncDateTo = null;
	 		this.loadData(null, null, null, null, null, null, null, null );
		 }
    }

    loadSyncDateFrom(itemChanged) {
    	if(itemChanged){
    		console.log("loadSyncDate", itemChanged.label, itemChanged.value);
    	}else {
    		console.log("loadSyncDate null values");
    	}
    	this.selectedSyncDateFrom = itemChanged;
    	this.reloadData();
    }

    loadSyncDateTo(itemChanged) {
    	if(itemChanged){
    		console.log("loadSyncDate", itemChanged.label, itemChanged.value);
    	}else {
    		console.log("loadSyncDate null values");
    	}
    	this.selectedSyncDateTo = itemChanged;
    	this.reloadData();
    }

    reloadData(){    
    	console.log("reloadData");	
      	if(this.selectedOrganisation && this.selectedOrganisation.value && this.selectedLunaAnFrom && this.selectedLunaAnFrom.value 
      		&& this.selectedLunaAnTo && this.selectedLunaAnTo.value && this.selectedSyncDateFrom && this.selectedSyncDateFrom.value 
      		&& this.selectedSyncDateTo && this.selectedSyncDateTo.value && this.selectedCont && this.selectedCont.value) {
    		console.log("reloadData NO null values");
			this.loadData(this.selectedOrganisation.value, this.selectedLunaAnFrom.value, this.selectedLunaAnTo.value, 
				this.selectedSyncDateFrom.value, this.selectedSyncDateTo.value, this.selectedCont.value, 0, this.getNrOfRows());
    	}
    	else {
	 		this.loadData(null, null, null, null, null, null, null, null );
    	}
    }


	cars: Car[];
    selectedCar1: Car;
    cols: any[];
	ngOnInit2() {
        // this.carService.getCarsSmall().then(cars => this.cars = cars);

        this.cars = [
        {vin: 'vin1', year: 2018, brand: 'Dacia', color: 'blue' },
        {vin: 'vin2', year: 2017, brand: 'BMW', color: 'red' },
        {vin: 'vin3', year: 2016, brand: 'VW', color: 'green' },
        {vin: 'vin4', year: 2015, brand: 'Seat', color: 'orange' }
        ];
        this.cols = [
            { field: 'vin', header: 'Vin' },
            { field: 'year', header: 'Year' },
            { field: 'brand', header: 'Brand' },
            { field: 'color', header: 'Color' }
        ];
    }
}

export class Car {
	vin: string;
	year: number;
	brand: string;
	color: string;
}