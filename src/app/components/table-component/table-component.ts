import {Component, OnInit} from '@angular/core';
import { TableComponentService } from './table-component-service';
import { NCont } from '../../model/n-cont';
import { Page } from '../../model/page';
 
import { HttpClient } from '@angular/common/http';

import {LazyLoadEvent} from 'primeng/primeng';
import {SelectItem} from 'primeng/api';
import { Organisation } from '../../model/organisation';
import { RestService } from '../../rest-service/rest-service';
import {TreeNode} from 'primeng/api';



@Component({
  selector: 'table-component',
  templateUrl: './table-component.html',
  //styleUrls: ['./table-component.css']
})
export class TableComponent  implements OnInit {
	ncontList: NCont[];
	ncontPage: Page<NCont>;
	jusList: string[] = ['1','2'];
    loading: boolean;

    // selectedOrganisationId: string = 'b596ac15-dfa5-441a-b764-96d9484a73f5';

    lunaAn: SelectItem[];
    selectedLunaAn: SelectItem;

    syncDateList: SelectItem[];
    selectedSyncDate: SelectItem;


    organisationList: SelectItem[];
    selectedOrganisation: SelectItem;

    selectedNcont: NCont;
	tableCols: any[];

	constructor(private tableComponentService: TableComponentService, private http: HttpClient,
				private restService: RestService) { }
	
	ngOnInit() {
		console.log("ngOnInit:");
		this.tableCols = [
			{ field: 'denumire', header: 'Tip balanta' },
			{ field: 'clasa', header: 'Clasa' },
			{ field: 'dateCreate', header: 'Data' },
			{ field: '', header: 'Luna' },
			{ field: 'soldic', header: 'SoldID' },
			{ field: 'soldic', header: 'SoldIC' },
			{ field: 'curentd', header: 'RulajD' },
			{ field: 'curentc', header: 'RulajC' },
			{ field: 'precedentd', header: 'PrecedentD' },
			{ field: 'precedentc', header: 'PrecedentC' },
			{ field: 'nsoldid', header: 'SoldFD' },
			{ field: 'nsoldic', header: 'SoldFC' }
		];


		this.initData();

		//TODO delete
		this.ngOnInit2();
	}

//TODO delete
	// getNContList2(): void {
	// 	this.tableComponentService.getNContList3()
 //     	.subscribe((ncontListVar: any) => this.ncontList = ncontListVar);
	// }

	getNrOfRows(): number{
		if(this.ncontPage) {
			return (this.ncontPage.pageSize);
		}else{
			return 10;
		}
	}
	getTotalRecords(): number{
		if(this.ncontPage) {
			return (this.ncontPage.total);
		}else{
			return 0;
		}
	}

	loadLazy(event: LazyLoadEvent) {
        this.loading = true;
        setTimeout(() => {
            if (this.tableComponentService) {            	
				this.loadData(this.selectedOrganisation.value, this.selectedLunaAn.value, this.selectedSyncDate.value, (event.first/event.rows), this.getNrOfRows());
                this.loading = false;
            }
        }, 1000);
    }


	isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}
    loadData(organisationId:string, selectedLunaAn:string, selectedSyncDate:string, page:number, pageSize:number) {
		console.log("loadData:");
		if (organisationId && selectedLunaAn && selectedSyncDate && this.isEmpty(page) && pageSize) {

	    	this.tableComponentService.getNContList1(organisationId, selectedLunaAn, selectedSyncDate, page, pageSize)
			     	.subscribe((ncontPage: Page<NCont>) => {this.ncontPage = ncontPage;
			     											console.log("ncontPage:" + ncontPage);
			     										   this.ncontList = ncontPage.items;
			     										   });
		}else {
			this.ncontList = [];
		}
    }

    initData() {
                             console.log("initData................." );
        this.restService.getOrganisationList('1')//this.user.userId)
             .subscribe((organisationList: Organisation[]) => {
                           this.organisationList = [];
                           if(organisationList){
                             for(var i=0; i<organisationList.length; i++){
                               this.organisationList.push({label:organisationList[i].alias, value:organisationList[i].id});
                             }
                             console.log("organisationList:" + organisationList);  
                          }  
                        if(this.organisationList){
						    this.changeOrganisation(this.organisationList[0]);
						}else {
							this.changeOrganisation(null);
						}
             }); 
    	 	
    }


  changeOrganisation(itemChanged){
    	if(itemChanged){
    		console.log("changeOrganisation", itemChanged.label, itemChanged.value);
    	} else {
    		console.log("changeOrganisation null values");
    	}
	this.selectedOrganisation = itemChanged;
      if(this.selectedOrganisation && this.selectedOrganisation.value) {
      	 this.tableComponentService.getLunaAnByOrganisationList(this.selectedOrganisation.value)
	     	.subscribe((lunaAnList: string[]) => {
	     								this.lunaAn = [];
					     				if(lunaAnList){
							     			for(var i=0; i<lunaAnList.length; i++){
							     				this.lunaAn.push({label:lunaAnList[i], value:lunaAnList[i]});
							     			}											
										}	
										console.log("lunaAnList:" + lunaAnList);
										if(this.lunaAn){
											this.loadLunaAn(this.lunaAn[0]);
										}else {
											this.loadLunaAn(null);
										}
	     });   
      } else {
      	this.lunaAn = [];
      	this.selectedLunaAn = null;
      	this.loadLunaAn(null);
      }
  }

    loadLunaAn(itemChanged) {
    	if(itemChanged){
    		console.log("loadLunaAn", itemChanged.label, itemChanged.value);
    	} else {
    		console.log("loadLunaAn null values");
    	}
		this.selectedLunaAn = itemChanged;
      	if(this.selectedOrganisation && this.selectedOrganisation.value && this.selectedLunaAn && this.selectedLunaAn.value) {
			this.tableComponentService.getSyncDateList(this.selectedOrganisation.value, this.selectedLunaAn.value)
		     	.subscribe((syncDateListR: string[]) => {
		     								this.syncDateList = [];
						     				if(syncDateListR){
								     			for(var i=0; i<syncDateListR.length; i++){
								     				this.syncDateList.push({label:syncDateListR[i], value:syncDateListR[i]});
	 											console.log("syncDateList: value"+ i + " =" + syncDateListR[i]);
								     			}
		     											// this.syncDateList = syncDateList;
	 											
											}
											console.log("syncDateList:" + syncDateListR);
 											if(this.syncDateList){
 												this.loadSyncDate(this.syncDateList[0]);
 											}else {
 												this.loadSyncDate(null);
 											}
											
		     	});   
		 } else {
		    this.syncDateList = [];
	 		this.selectedSyncDate = null;
	 		this.loadData(null, null, null, null, null );
		 }
    }

    loadSyncDate(itemChanged) {
    	if(itemChanged){
    		console.log("loadSyncDate", itemChanged.label, itemChanged.value);
    	}else {
    		console.log("loadSyncDate null values");
    	}
    	this.selectedSyncDate = itemChanged;
      	if(this.selectedOrganisation && this.selectedOrganisation.value && this.selectedLunaAn && this.selectedLunaAn.value && this.selectedSyncDate && this.selectedSyncDate.value) {
    		this.loadData(this.selectedOrganisation.value, this.selectedLunaAn.value, this.selectedSyncDate.value, 0, this.getNrOfRows() );
    	}
    	else {
	 		this.loadData(null, null, null, null, null );
    	}
  //   	this.tableComponentService.getNContList1(organisationId, selectedLunaAn, selectedSyncDate, page, pageSize)
		//      	.subscribe((ncontPage: Page<NCont>) => {this.ncontPage = ncontPage;
		//      										   this.ncontList = ncontPage.items;
		//      										   });

		// this.loadData(this.selectedOrganisationId, this.selectedLunaAn, this.selectedSyncDate, 1, this.getNrOfRows());
    }


	cars: Car[];
    selectedCar1: Car;
    cols: any[];
	ngOnInit2() {
        // this.carService.getCarsSmall().then(cars => this.cars = cars);

        this.cars = [
        {vin: 'vin1', year: 2018, brand: 'Dacia', color: 'blue' },
        {vin: 'vin2', year: 2017, brand: 'BMW', color: 'red' },
        {vin: 'vin3', year: 2016, brand: 'VW', color: 'green' },
        {vin: 'vin4', year: 2015, brand: 'Seat', color: 'orange' }
        ];
        this.cols = [
            { field: 'vin', header: 'Vin' },
            { field: 'year', header: 'Year' },
            { field: 'brand', header: 'Brand' },
            { field: 'color', header: 'Color' }
        ];
    }
}

export class Car {
	vin: string;
	year: number;
	brand: string;
	color: string;
}