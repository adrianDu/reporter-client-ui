import { Injectable } from '@angular/core';
import { NCont } from '../../model/n-cont';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import {TreeNode} from 'primeng/api';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Origin':'*'})
};

@Injectable()
export class TableComponentService {

	//TODO
  private serviceUrl = '/report/ncont';

  constructor(
    private http: HttpClient) { }


  getNContTreeNodes(organisationIdV:string, selectedLunaAnV:string, selectedSyncDateV:string, pageV:number, pageSizeV:number): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV)
    .set('accountDate', selectedLunaAnV)
    .set('syncDate', selectedSyncDateV)
    .set('page', pageV.toString())
    .set('pageSize', pageSizeV.toString());
   return this.http.get('http://reporter.local:7788/report/ncont/filters/treepage'
     , { params: body}
     );
  }

  getNContTreeNodeLeafs(organisationIdV:string, selectedLunaAnV:string, selectedSyncDateV:string, codParinteV:number): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV)
    .set('accountDate', selectedLunaAnV)
    .set('syncDate', selectedSyncDateV)
    .set('codParinte', codParinteV.toString())
   return this.http.get('http://reporter.local:7788/report/ncont/filters/treepage/children'
     , { params: body}
     );
  }

  getNContList1(organisationIdV:string, selectedLunaAnV:string, selectedSyncDateV:string, pageV:number, pageSizeV:number): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV)
    .set('accountDate', selectedLunaAnV)
    .set('syncDate', selectedSyncDateV)
    .set('page', pageV.toString())
    .set('pageSize', pageSizeV.toString());
   return this.http.get('http://reporter.local:7788/report/ncont/filters'
     , { params: body}
     );
  }

  getHNContList(organisationIdV:string, selectedLunaAnFromV:string, selectedLunaAnToV:string, selectedSyncDateFromV:string, 
    selectedSyncDateToV:string, selectedSimbolV:string, pageV:number, pageSizeV:number): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV)
    .set('accountDateFrom', selectedLunaAnFromV)
    .set('accountDateTo', selectedLunaAnToV)
    .set('syncDateFrom', selectedSyncDateFromV)
    .set('syncDateTo', selectedSyncDateToV)
    .set('simbol', selectedSimbolV)
    .set('page', pageV.toString())
    .set('pageSize', pageSizeV.toString());
    console.log("getHNContList");
   return this.http.get('http://reporter.local:7788/report/ncont/cont-filters'
     , { params: body}
     );
  }

  getConturiByOrganisationList(organisationIdV:string): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV);
   return this.http.get('http://reporter.local:7788/report/ncont/conturi'
     , { params: body}
     );
  }

  getLunaAnByOrganisationList(organisationIdV:string): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV);
   return this.http.get('http://reporter.local:7788/report/ncont/accountDate'
     , { params: body}
     );
  }

  getSyncDateList(organisationIdV:string, selectedLunaAnV:string): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV)
    .set('accountDate', selectedLunaAnV);
   return this.http.get('http://reporter.local:7788/report/ncont/accountDateSync'
     , { params: body}
     );
  }

  getSyncDateListFromTo(organisationIdV:string, selectedLunaAnFromV:string, selectedLunaAnToV:string): Observable<any> {
   const body = new HttpParams()
    .set('organisationId', organisationIdV)
    .set('accountDateFrom', selectedLunaAnFromV)
    .set('accountDateTo', selectedLunaAnToV);
   return this.http.get('http://reporter.local:7788/report/ncont/accountDateSync/from-to'
     , { params: body}
     );
  }

  getNContList2(): Observable<any> {
   return this.http.get('http://reporter.local:7788/report/ncont/filters/page'
     // , httpOptions
     , {
     params: {
       organisationId: 'b596ac15-dfa5-441a-b764-96d9484a73f5'
     }}
     );
   // organisationId=18e69410-b057-4c50-8936-20d428b6358f
   // return this.http.get('http://reporter.local:7788/report/ncont');


  
  }
  // getNContList(): NCont[] {
  // 	return NContList;
  // }
  // getNContListObservable (): Observable<NCont[]> {
  //   return this.http.get<NCont[]>(this.serviceUrl)
  //     .pipe(
  //       tap(nconts => this.log(`fetched ncont list`)),
  //       catchError(this.handleError('getNContListObservable', []))
  //     )
  //     ;
  // }



  getNContList3(): Observable<any> {
   return this.http.get('http://reporter.local:7788/report/ncont/filters'
     // , httpOptions
     , {
     params: {
       organisationId: 'b596ac15-dfa5-441a-b764-96d9484a73f5'
     }}
     );
 }

}
// export const NContList: NCont[] = [
//   {id: '1', iCod: 1, iDenumire: '1.0'},
//   {id: '2', iCod: 2, iDenumire: '2.0'}
// ];
