import {Component, OnInit} from '@angular/core';
import { TableComponentService } from './table-component-service';
import { NCont } from '../../model/n-cont';
import { Page } from '../../model/page';
import { TreeNodeC } from '../../model/tree-node';
 
import { HttpClient } from '@angular/common/http';

import { LazyLoadEvent } from 'primeng/primeng';
import { SelectItem } from 'primeng/api';
import { Organisation } from '../../model/organisation';
import { RestService } from '../../rest-service/rest-service';
import { TreeNode } from 'primeng/api';
import { NContShareDataService } from '../shared/n-cont.sharedata.service';
import {Routes, RouterModule, Router} from "@angular/router";




@Component({
  selector: 'treetable-component',
  templateUrl: './treetable-component.html',
  //styleUrls: ['./table-component.css']
})
export class TreeTableComponent  implements OnInit {
	ncontList: TreeNodeC[];
	ncontPage: Page<TreeNodeC>;
    loading: boolean = false;

    lunaAn: SelectItem[];
    selectedLunaAn: SelectItem;

    syncDateList: SelectItem[];
    selectedSyncDate: SelectItem;

    organisationList: SelectItem[];
    selectedOrganisation: SelectItem;

    selectedNcont: TreeNodeC;
	tableCols: any[];
	constructor(private tableComponentService: TableComponentService, private http: HttpClient,
				private restService: RestService,
				private shareDataService: NContShareDataService,
				private router: Router) { }
	
	ngOnInit() {
		console.log("ngOnInit:");
		this.tableCols = [
			{ field: 'denumire', header: 'Tip balanta' },
			{ field: 'clasa', header: 'Clasa' },
			{ field: 'dateCreate', header: 'Data' },
			{ field: '', header: 'Luna' },
			{ field: 'soldic', header: 'SoldID' },
			{ field: 'soldic', header: 'SoldIC' },
			{ field: 'curentd', header: 'RulajD' },
			{ field: 'curentc', header: 'RulajC' },
			{ field: 'precedentd', header: 'PrecedentD' },
			{ field: 'precedentc', header: 'PrecedentC' },
			{ field: 'nsoldid', header: 'SoldFD' },
			{ field: 'nsoldic', header: 'SoldFC' }
		];


		this.initData();

		//TODO delete
		this.ngOnInit2();
	}

    nodeSelect(event) {
    	console.log("on select:", JSON.stringify(event.node.data));
    	const node = event.node;
    	this.shareDataService.setNContDto(this.selectedOrganisation.value, this.selectedLunaAn.value, this.selectedSyncDate.value, node.data.simbol);
    	this.router.navigate(['/h-balanta']);
        // this.msgs = [];
        // this.msgs.push({severity: 'info', summary: 'Node Selected', detail: event.node.data.name});
    }

    nodeUnselect(event) {
        // this.msgs = [];
        // this.msgs.push({severity: 'info', summary: 'Node Unselected', detail: event.node.data.name});
    }
//TODO delete
	// getNContList2(): void {
	// 	this.tableComponentService.getNContList3()
 //     	.subscribe((ncontListVar: any) => this.ncontList = ncontListVar);
	// }

	getNrOfRows(): number{
		if(this.ncontPage) {
			return (this.ncontPage.pageSize);
		}else{
			return 10;
		}
	}
	getTotalRecords(): number{
		if(this.ncontPage) {
			return (this.ncontPage.total);
		}else{
			return 0;
		}
	}

	loadLazy(event: LazyLoadEvent) {
        this.loading = true;
        setTimeout(() => {
            if (this.tableComponentService) {            	
				this.loadData(this.selectedOrganisation.value, this.selectedLunaAn.value, this.selectedSyncDate.value, (event.first/event.rows), this.getNrOfRows());
                this.loading = false;
            }
        }, 1000);
    }

	onNodeExpand(event) {
        this.loading = true;
        console.log("event on node expanded:"+event,event.node.data.cod);
        // console.log(JSON.stringify(event));
        setTimeout(() => {
            const node = event.node;
            if (this.tableComponentService) {            	
				this.loadLeafData(this.selectedOrganisation.value, this.selectedLunaAn.value, this.selectedSyncDate.value, node.data.cod, node);
                this.loading = false;
            }
        }, 1000);
    }


	isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}
    loadData(organisationId:string, selectedLunaAn:string, selectedSyncDate:string, page:number, pageSize:number) {
		console.log("loadData:");
		if (organisationId && selectedLunaAn && selectedSyncDate && this.isEmpty(page) && pageSize) {

	    	this.tableComponentService.getNContTreeNodes(organisationId, selectedLunaAn, selectedSyncDate, page, pageSize)
			     	.subscribe((ncontPage: Page<TreeNodeC>) => {this.ncontPage = ncontPage;
			     											console.log("ncontPage:" + ncontPage);
			     										   this.ncontList = ncontPage.items;
			     										   });
		}else {
			this.ncontList = [];
		}
    }

    loadLeafData(organisationId:string, selectedLunaAn:string, selectedSyncDate:string, codParinte:number, treeNode:TreeNodeC) {
		console.log("loadData leafs:");
		if (organisationId && selectedLunaAn && selectedSyncDate && codParinte) {

	    	this.tableComponentService.getNContTreeNodeLeafs(organisationId, selectedLunaAn, selectedSyncDate, codParinte)
			     	.subscribe((ncontList: TreeNodeC[]) => {
			     											treeNode.children = ncontList;
            												this.ncontList = [...this.ncontList];
			     											console.log("lista leafs:", JSON.stringify(treeNode.children));
			     											// console.log("lista leafs:", JSON.stringify(ncontList));
			     											// return ncontList;
			     										   });
		}
    }

    initData() {
                             console.log("initData................." );
        this.restService.getOrganisationList('1')//this.user.userId)
             .subscribe((organisationList: Organisation[]) => {
                           this.organisationList = [];
                           if(organisationList){
                             for(var i=0; i<organisationList.length; i++){
                               this.organisationList.push({label:organisationList[i].alias, value:organisationList[i].id});
                             }
                             console.log("organisationList:" + organisationList);  
                          }  
                        if(this.organisationList){
						    this.changeOrganisation(this.organisationList[0]);
						}else {
							this.changeOrganisation(null);
						}
             }); 
    	 	
    }


  changeOrganisation(itemChanged){
    	if(itemChanged){
    		console.log("changeOrganisation", itemChanged.label, itemChanged.value);
    	} else {
    		console.log("changeOrganisation null values");
    	}
	this.selectedOrganisation = itemChanged;
      if(this.selectedOrganisation && this.selectedOrganisation.value) {
      	 this.tableComponentService.getLunaAnByOrganisationList(this.selectedOrganisation.value)
	     	.subscribe((lunaAnList: string[]) => {
	     								this.lunaAn = [];
					     				if(lunaAnList){
							     			for(var i=0; i<lunaAnList.length; i++){
							     				this.lunaAn.push({label:lunaAnList[i], value:lunaAnList[i]});
							     			}											
										}	
										console.log("lunaAnList:" + lunaAnList);
										if(this.lunaAn){
											this.loadLunaAn(this.lunaAn[0]);
										}else {
											this.loadLunaAn(null);
										}
	     });   
      } else {
      	this.lunaAn = [];
      	this.selectedLunaAn = null;
      	this.loadLunaAn(null);
      }
  }

    loadLunaAn(itemChanged) {
    	if(itemChanged){
    		console.log("loadLunaAn", itemChanged.label, itemChanged.value);
    	} else {
    		console.log("loadLunaAn null values");
    	}
		this.selectedLunaAn = itemChanged;
      	if(this.selectedOrganisation && this.selectedOrganisation.value && this.selectedLunaAn && this.selectedLunaAn.value) {
			this.tableComponentService.getSyncDateList(this.selectedOrganisation.value, this.selectedLunaAn.value)
		     	.subscribe((syncDateListR: string[]) => {
		     								this.syncDateList = [];
						     				if(syncDateListR){
								     			for(var i=0; i<syncDateListR.length; i++){
								     				this.syncDateList.push({label:syncDateListR[i], value:syncDateListR[i]});
	 											console.log("syncDateList: value"+ i + " =" + syncDateListR[i]);
								     			}
		     											// this.syncDateList = syncDateList;
	 											
											}
											console.log("syncDateList:" + syncDateListR);
 											if(this.syncDateList){
 												this.loadSyncDate(this.syncDateList[0]);
 											}else {
 												this.loadSyncDate(null);
 											}
											
		     	});   
		 } else {
		    this.syncDateList = [];
	 		this.selectedSyncDate = null;
	 		this.loadData(null, null, null, null, null );
		 }
    }

    loadSyncDate(itemChanged) {
    	if(itemChanged){
    		console.log("loadSyncDate", itemChanged.label, itemChanged.value);
    	}else {
    		console.log("loadSyncDate null values");
    	}
    	this.selectedSyncDate = itemChanged;
      	if(this.selectedOrganisation && this.selectedOrganisation.value && this.selectedLunaAn && this.selectedLunaAn.value && this.selectedSyncDate && this.selectedSyncDate.value) {
    		this.loadData(this.selectedOrganisation.value, this.selectedLunaAn.value, this.selectedSyncDate.value, 0, this.getNrOfRows() );
    	}
    	else {
	 		this.loadData(null, null, null, null, null );
    	}
  //   	this.tableComponentService.getNContList1(organisationId, selectedLunaAn, selectedSyncDate, page, pageSize)
		//      	.subscribe((ncontPage: Page<NCont>) => {this.ncontPage = ncontPage;
		//      										   this.ncontList = ncontPage.items;
		//      										   });

		// this.loadData(this.selectedOrganisationId, this.selectedLunaAn, this.selectedSyncDate, 1, this.getNrOfRows());
    }


	files: TreeNode[];
    cols: any[];
	ngOnInit2() {
        // this.carService.getCarsSmall().then(cars => this.cars = cars);
        console.log("files loaded");
        this.files = [
        	{
			    "data":
			    [  
			        {  
			            "data":{  
			                "name":"Documents",
			                "size":"75kb",
			                "type":"Folder"
			            },
			            "children":[
			                {  
			                    "data":{  
			                        "name":"Work",
			                        "size":"55kb",
			                        "type":"Folder"
			                    },
			                    "children":[  
			                        {  
			                            "data":{  
			                                "name":"Expenses.doc",
			                                "size":"30kb",
			                                "type":"Document"
			                            }
			                        },
			                        {  
			                            "data":{  
			                                "name":"Resume.doc",
			                                "size":"25kb",
			                                "type":"Resume"
			                            }
			                        }
			                    ]
			                },
			                {  
			                    "data":{  
			                        "name":"Home",
			                        "size":"20kb",
			                        "type":"Folder"
			                    },
			                    "children":[  
			                        {  
			                            "data":{  
			                                "name":"Invoices",
			                                "size":"20kb",
			                                "type":"Text"
			                            }
			                        }
			                    ]
			                }
			            ]
			        },
			        {  
			            "data":{  
			                "name":"Pictures",
			                "size":"150kb",
			                "type":"Folder"
			            },
			            "children":[  
			                {  
			                    "data":{  
			                        "name":"barcelona.jpg",
			                        "size":"90kb",
			                        "type":"Picture"
			                    }
			                },
			                {  
			                    "data":{  
			                        "name":"primeui.png",
			                        "size":"30kb",
			                        "type":"Picture"
			                    }
			                },
			                {  
			                    "data":{  
			                        "name":"optimus.jpg",
			                        "size":"30kb",
			                        "type":"Picture"
			                    }
			                }
			            ]
			        }
			    ]
			}
        ];
        this.cols = [
            { field: 'vin', header: 'Vin' },
            { field: 'year', header: 'Year' },
            { field: 'brand', header: 'Brand' },
            { field: 'color', header: 'Color' }
        ];


        this.files = [];
        for(let i = 0; i < 50; i++) {
            let node = {
                data:{  
                    name: 'Item ' + i,
                    size: Math.floor(Math.random() * 1000) + 1 + 'kb',
                    type: 'Type ' + i
                },
                children: [
                    {
                        data: {  
                            name: 'Item ' + i + ' - 0',
                            size: Math.floor(Math.random() * 1000) + 1 + 'kb',
                            type: 'Type ' + i
                        }
                    }
                ]
            };

            this.files.push(node);
        }

        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'size', header: 'Size' },
            { field: 'type', header: 'Type' }
        ];
  }


}

export class Car {
	vin: string;
	year: number;
	brand: string;
	color: string;
}