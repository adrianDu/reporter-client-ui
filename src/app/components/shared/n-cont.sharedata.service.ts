import { Injectable } from '@angular/core';
import { NCont } from '../../model/n-cont';
import { NContDto } from '../../model/n-cont-dto';



@Injectable()
export class NContShareDataService {
	ncontDto: NContDto;

	public setNContDto(organisationId:string,lunaAn:string, syncDate:string, simbol:number){
		this.ncontDto = new NContDto();
		this.ncontDto.organisationId = organisationId;
		this.ncontDto.lunaAn = lunaAn;
		this.ncontDto.syncDate = syncDate;
		this.ncontDto.simbol = simbol;

	}

	public emptyNcontDto(){
		this.ncontDto = null;
	}


}
