import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { User2 } from '../model/user2';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/Rx';

@Injectable()
export class RestService {
	//TODO
  private serviceUrl = '/report/ncont'; 
  private token;
  private headers22 = new HttpHeaders();
  private headersContent = new HttpHeaders();
  
  constructor(
    private http: HttpClient) { }

  logIn(usernameV: string,passwordV: string): Observable<any>  {

   // return this.http.get('http://reporter.local:8080/report/ncont', httpOptions);
   // organisationId=18e69410-b057-4c50-8936-20d428b6358f
   const body = new HttpParams()
    .set('username', usernameV)
    .set('password', passwordV);
      console.log('enter method logIn ' + body.toString());
   const user = new User2();
   user.username = usernameV;
   user.password = passwordV;


      let headers = new HttpHeaders().set('Content-Type', 'application/json')
      // let headers = new Headers({ 'Content-Type': 'application/json'});
      let options = { headers: headers };
 this.headersContent.append('Content-Type','application/json');
   return this.http.post('http://reporter.local:7788/report/ncont/authenticate', 
     // { params: body} ,
     // {username: usernameV, password: passwordV} ,
     user,
     options
      // {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    ).map((response: any) =>{
      console.log('loging authenticate');
      console.log('response:'+ JSON.parse(response));
      return true;
    })

   // this.headersContent.append('Content-Type','application/json');
   // return this.http.post('http://reporter.local:7788/authenticate', {'username': username ,'password': password},
   //    {headers: new HttpHeaders().set('Content-Type', 'application/json')
   //  }).map((response: any) =>{
   //    console.log('loging authenticate');
   //    console.log('response:'+ JSON.parse(response));
   //    return true;
   //  })

   ;
   //   	.subscribe((response: any) => {
   //   		// this.token = response.token;
   //       console.log('loging authenticate');
   //   		console.log('loging token:' + this.token);
			// this.headers22.append('X-Authorization', this.token); 
   //   		// return this.http.get('http://reporter.local:8080/user/authenticated',  {
   //   		// 	headers: this.headers22
   //   		// })
		 //     // 	.subscribe((response: any) => {console.log('logged in')})
   //   	});

  }

  getOrganisationList(userId:string): Observable<any> {
   const body = new HttpParams()
    .set('userId', userId);
   return this.http.get('http://reporter.local:7788/report/ncont/organisations'
     , { params: body}
     );
  }
 
}