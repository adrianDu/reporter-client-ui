import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TableComponent } from './components/table-component/table-component';
import { TreeTableComponent } from './components/table-component/treetable-component'
import { HNcontComponent } from './components/table-component/h-ncont-component';
// import { TreeTableComponent } from './components/table-component/treetable-component';


const routes: Routes = [
	  {
	    path: '',
	    component: TreeTableComponent,
	  },
	  {
	    path: 'balanta',
	    component: TreeTableComponent,
	  },

	  // map 
	  {
	    path: 'h-balanta',
	    component: HNcontComponent,
	  }
	];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
