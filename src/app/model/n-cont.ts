

export class NCont {
	id?: string;	
	dateCreate?: Date;
	dateUpdate?: Date;
	ncontFile?: string;
	cod?: number;
	clasa?: number;
	simbol?: string;
	denumire?: string;
	name?: string;
	tip?: number;
	nivel?: number;
	nranalitice?: number;
	codparinte?: number;
	soldid?: number;
	nsoldid?: number;
	soldic?: number;
	nsoldic?: number;
	precedentd?: number;
	nprecedentd?: number;
	precedentc?: number;
	nprecedentc?: number;
	curentd?: number;
	curentc?: number;

}