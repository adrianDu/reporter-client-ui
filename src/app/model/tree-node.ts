import {TreeNode} from 'primeng/api';

export class TreeNodeC implements TreeNode {
 data?: any;
 children?: TreeNodeC[];
 leaf?: boolean;
 expanded?: boolean;
}