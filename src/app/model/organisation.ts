export class Organisation {
	id?: string;
	date_create?: Date;
	date_update?: Date;
	code?: string;
	name?: string;
	alias?: string;
	active?: boolean;
	registryCode?: string;
	fund?: number;
	primaryPhoneNumber?: string;
	mobilePhoneNumber?: string;
	faxNumber?: string;
	parentOrganisationId?: string;
	remoteServerSyncConfigId?: string;
}