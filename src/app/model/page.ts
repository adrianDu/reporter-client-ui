 export class Page<T> {
	total?: number;	
	page?: number;
	pageSize?: number;
	items?: T[];

}