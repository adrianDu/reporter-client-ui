export class User {
	id?: string;
	active?: boolean;
	username?: string;
	organisationId?: string;
	contactDetailsId?: string;
	lastActivity?: Date;
	role?: string;

}
